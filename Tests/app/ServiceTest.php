<?php

namespace DeejayPoolBundle\Tests;

use PHPUnit\Framework\TestCase;

/**
 * Class ServiceTest
 */
class ServiceTest extends TestCase
{
    private $container;

    protected function setUp()
    {
        $kernel = new \AppKernel('test', true);
        $kernel->boot();

        $this->container = $kernel->getContainer();
    }

    public function testServiceIsDefinedInContainer()
    {
        $service = $this->container->get(\DeejayPoolBundle\DeejayPoolBundle::PROVIDER_AVD);

        $this->assertInstanceOf('DeejayPoolBundle\Provider\AvDistrictProvider', $service);
    }
}
