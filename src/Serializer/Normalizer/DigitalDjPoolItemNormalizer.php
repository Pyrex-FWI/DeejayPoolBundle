<?php

namespace DeejayPoolBundle\Serializer\Normalizer;

use DeejayPoolBundle\Entity\AvdItem;
use DeejayPoolBundle\Entity\DdpItem;
use DeejayPoolBundle\Provider\DigitalDjPoolProvider;
use DigitalDjPoolBundle\Exception\DownloadLinkNotFoundException;
use GuzzleHttp\Client;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\VarDumper\VarDumper;

/**
 * Class DigitalDjPoolItemNormalizer
 *
 * @package DeejayPoolBundle\Serializer\Normalizer
 * @author Christophe Pyree <yemistikris@hotmail.fr>
 */
class DigitalDjPoolItemNormalizer extends AbstractNormalizer
{
    const DDPITEM = 'DdpItem';

    /**
     * Denormalizes data back into an object of the given class.
     *
     * @param array  $dataRaw data to restore
     * @param string $class   the expected class to instantiate
     * @param string $format  format the given data was extracted from
     * @param array  $context options available to the denormalizer
     *
     * @throws DownloadLinkNotFoundException
     *
     * @return AvdItem
     */
    public function denormalize($dataRaw, $class, $format = null, array $context = array())
    {
        $data = new Crawler($dataRaw);
        $ddpItem = new DdpItem();

        if ($data->filter('a.ddjp-download')->count() === 0 || $data->filter('a.ddjp-download')->attr('data-href') == '#') {
            throw new DownloadLinkNotFoundException();
        }
        $dlLink = $this->retrieveDownloadLink($context, $data);
        $ddpItem->setDownloadlink($dlLink);
        if ($data->filter('input.hid-song-id')->count()) {
            $ddpItem->setItemId($data->filter('input.hid-song-id')->attr('value'));
        }

        if ($data->filter('input.hid-song-artist')->count()) {
            $ddpItem->setArtist($data->filter('input.hid-song-artist')->attr('value'));
        }

        if ($data->filter('input.hid-song-title')->count()) {
            $ddpItem->setTitle($data->filter('input.hid-song-title')->attr('value'));
        }

        if ($data->filter('input.hid-song-version')->count()) {
            $ddpItem->setVersion($data->filter('input.hid-song-version')->attr('value'));
        }

        if ($data->filter('.col-bpm')->count()) {
            $ddpItem->setBpm($data->filter('.col-bpm')->attr('value'));
        }

        $ddpItem->setReleaseDate(new \DateTime());

        return $ddpItem;
    }

    /**
     * Checks whether the given class is supported for denormalization by this normalizer.
     *
     * @param mixed  $data   data to denormalize from
     * @param string $type   the class to which the data should be denormalized
     * @param string $format the format being deserialized from
     *
     * @return bool
     */
    public function supportsDenormalization($data, $type, $format = null)
    {
        return self::DDPITEM === $type && is_string($data);
    }

    /**
     * Normalizes an object into a set of arrays/scalars.
     *
     * @param object $object  object to normalize
     * @param string $format  format the normalization result will be encoded as
     * @param array  $context Context options for the normalizer
     *
     * @return array|string|bool|int|float|null
     */
    public function normalize($object, $format = null, array $context = array())
    {
        return;
    }

    /**
     * Checks whether the given class is supported for normalization by this normalizer.
     *
     * @param mixed  $data   data to normalize
     * @param string $format the format being (de-)serialized from or into
     *
     * @return bool
     */
    public function supportsNormalization($data, $format = null)
    {
        return false;
    }

    /**
     * @param array $context
     * @param       $data
     */
    public function retrieveDownloadLink(array $context, $data)
    {
        /** @var Client $client */
        $client = isset($context['client']) && $context['client'] instanceof Client ? $context['client']: null;
        $apiUri = sprintf('%s%s', DigitalDjPoolProvider::HTTPS_DIGITALDJPOOL_COM, $data->filter('a.ddjp-download')->attr('data-href'));

        $response = $client->get(
            $apiUri,
            [
                'cookies' => clone $context['cookie'],
                'headers' => [
                    'Referer' => 'https://digitaldjpool.com/RecordPool/Search',
                    'Upgrade-Insecure-Requests' => 1,
                    'Accept' => 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
                    'Accept-Encoding' => 'gzip, deflate, sdch',
                    'Accept-Language' => 'fr-FR,fr;q=0.8,en-US;q=0.6,en;q=0.4',
                    'X-Requested-Wit' => 'XMLHttpRequest',
                ],
            ]
        );
        $apiReturn = $response->getBody()->getContents();
        $data = new Crawler($apiReturn);

        return $data->filter('http_mp3_download')->text();
    }
}
