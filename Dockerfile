FROM php:7.1

COPY --from=composer:1.7 /usr/bin/composer /usr/bin/composer

RUN apt-get update && apt-get install -y \
    libmcrypt-dev \
    libzip-dev \
    libxml2-dev \
    libxslt-dev \
    zip \
    git \
    && docker-php-ext-install -j$(nproc) \
    iconv \
    mcrypt \
    xsl \
    && pecl install \
    xdebug-2.6.1 \
    zip \
    && docker-php-ext-enable \
    zip \
    xdebug \
    opcache

#DEV Only
COPY ./docker/docker-php-ext-xdebug.ini /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
COPY ./docker/php.ini /usr/local/etc/php/

RUN rm -r /var/lib/apt/lists/*

