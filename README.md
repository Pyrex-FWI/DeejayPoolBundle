# DeejayPoolBundle

[![Software License](https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square)](LICENSE.txt)
[![Build Status](https://travis-ci.org/Pyrex-FWI/DigitalDjPoolBundle.svg?branch=master)](https://travis-ci.org/Pyrex-FWI/DeejayPoolBundle)
[![Codacy Badge](https://www.codacy.com/project/badge/96ed127edb5c409e99550057b49025f0)](https://www.codacy.com/app/yemistikris/DeejayPoolBundle)

Getting started
===============

## Installing DeejayPoolBundle

Update your composer.json

```json
"require-dev": {
    "pyrex-fwi/deejaypool-bundle": "dev-master"
}
```
or 

`composer require pyrex-fwi/deejaypool-bundle`

Update your `app/config/AppKernel.php` file

```php
<?php

public function registerBundles()
{
    $bundles = [
        // ...
        new DeejayPoolBundle\DeejayPoolBundle()
        // ...
    ];

    // ...
}

?>
```php

Add your account information into config.yml

```yaml
deejay_pool:
    providerName:
        credentials:
            login: replace_with_yours
            password: replace_with_yours
        configuration:
            root_path: /replace/by/writable/path/destination

```

## Supported providers

* AvDistrict (Videos, [Create account](http://www.avdistrict.net/Account/Register))
* DigitalDjPool (Musics, [Create account](https://digitaldjpool.com/Account.aspx/Register))
* Smash Vision (Videos, [Create account](https://www.smashvision.net/Home/Register)
* Franchise Record Pool(Musics and videos, [Create account](http://www.franchiserecordpool.com)

```yaml
deejay_pool:
    av_district:
        credentials:
            login:    %av_district.credentials.login%
            password: %av_district.credentials.password%
        configuration:
            root_path: %av_district.configuration.root_path%

    franchise_pool_audio:
        credentials:
            login:    brubruno
            password: maladede
        configuration:
            root_path: %franchise_pool.configuration.root_path%

    franchise_pool_video:
        credentials:
            login:    %franchise_pool.credentials.login%
            password: %franchise_pool.credentials.password%
        configuration:
            root_path: %franchise_pool.configuration.root_path%
    
    smashvision:
        credentials:
            login:    %smashvision.credentials.login%
            password: %smashvision.credentials.password%

        configuration:
            root_path: %smashvision.configuration.root_path%
```

## Console usages:

-  deejay:discover                         (Discover prodivers)
-  deejay:pool:status                      (Check account credentials)
-  php app/console deejay:pool:status franchise_pool_audio -vvv
-  deejay:pool:download                    (Download files from a specific provider)
 - download files 
 - search files

Events
======

Session events
==============

| Name              |                                                  |
|:------------------|:------------------------------------------------:|
| SESSION_OPENED    | After successful PoolProviderInterface::open()   |
| SESSION_OPEN_ERROR| When error occur on PoolProviderInterface::open()|
| SESSION_CLOSED    | After successful PoolProviderInterface::close()  |


Item events
===========

| Name                            |                                                                                  |
|:------------------------------- |:--------------------------------------------------------------------------------:|
| ITEMS_POST_GETLIST              | After successful page items normalization in PoolProviderInterface::getItems()   |
| ITEM_PRE_DOWNLOAD               | Triggered inside PoolProviderInterface::downloadItem() before true request       |
| ITEM_SUCCESS_DOWNLOAD           | Dispatched when an item is correctly downloaded                                  |
| ITEM_ERROR_DOWNLOAD             | Dispatched when itemCanBeDownloaded/ItemPreDownload propagation is stopped/Http Download Error /  |
| SEARCH_ITEM_LOCALY              |                                   |



#### Run tests
- ant install-deps
- ant
- vendor/bin/phpunit -c phpunit.xml --debug --verbose --exclude online
- vendor/bin/phpunit -c phpunit.xml --debug --verbose --coverage-html Tests/TestData/Coverage --exclude online
- vendor/bin/phpunit -c phpunit.xml --debug --verbose --coverage-html Tests/TestData/Coverage --debug --stop-on-error -v

### Run tests with real credentials

- export credential login and password

- vendor/bin/phpunit -c phpunit.xml --group online --debug --verbose

- gitlab-runner exec docker test:Fonctionnel


![Pseudoarchi](src/Resources/docs/plantuml/assets/archi.png)


![Pseudoarchi](src/Resources/docs/plantuml/assets/diagram.png)
docker build -t pyrex-fwi/ddp-bundle . 
docker run -it --rm -v "$PWD":/usr/var/app -w /usr/var/app pyrex-fwi/ddp-bundle bash
docker run -it --rm -v "$PWD":/usr/var/app -w /usr/var/app pyrex-fwi/ddp-bundle php tests/app/console.php s:w:c -v --no-mail
docker run -it --rm -v ~/PhpstormProjects/webservice-collection-bundle/:/usr/var/app -v ~/.cache/composer:/home/smile/.composer/cache -w /usr/var/app yemistikris/wsc bash
docker run -it --rm -v "$PWD":/usr/var/app -w /usr/var/app pyrex-fwi/ddp-bundle ant
docker run -it --rm -v "$PWD":/usr/var/app -v /home/chpyr/.cache/composer:/.composer --user 1000:1000 -w /usr/var/app pyrex-fwi/ddp-bundle bash
docker run -it --rm -v "$PWD":/usr/var/app -v /home/chpyr/.cache/composer:/.composer --user 1000:1000 -w /usr/var/app pyrex-fwi/ddp-bundle composer update --prefer-lowest --prefer-stable
docker run -it --rm -v "$PWD":/usr/var/app -v /home/chpyr/.cache/composer:/.composer --user 1000:1000 -w /usr/var/app pyrex-fwi/ddp-bundle vendor/bin/phpunit --no-coverage


### Docker

eval `ssh-agent`

```
docker run -it --rm -v "$PWD":/usr/var/app -v /home/chpyr/.cache/composer:/.composer --user 1000:1000 --env "XDEBUG_CONFIG=remote_host=172.17.0.1 remote_port=9000" --env "PHP_IDE_CONFIG=serverName=DEBUG" -w /usr/var/app pyrex-fwi/ddp-bundle vendor/bin/phpunit 

docker run -it --rm \
-v /c/Users/Kris/PhpstormProjects/DeejayPoolBundle:/usr/var/app \
-v /C/Users/Kris/AppData/Local/Composer:/.composer \
-w /usr/var/app pyrex-fwi/ddp-bundle bash
/sbin/ip route|awk '/default/ { print $3 }' ==> 172.17.0.1

--env "XDEBUG_CONFIG=remote_host=172.17.0.1 remote_port=9000" --env "PHP_IDE_CONFIG=serverName=DEBUG"
export XDEBUG_CONFIG="idekey=DEBUG remote_host=172.17.0.1 remote_port=9000" && export PHP_IDE_CONFIG="serverName=DEBUG"

```
